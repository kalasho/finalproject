//**********************************
// Omar Kalash, Stephanie Vail, Allie Jones
// Final Project
// May5, 2015
// Purpose: To make a "Who Wants to be a Millionaire" type game.
//***********************************
import java.util.Scanner;
import java.util.ArrayList;


public class FinalQuestions
{

    String [] Q = new String [5];
    boolean millionaire= true;
    String answer1, answer2, answer3, answer4, answer5;
    String answer;


    //****
    // Constructor
    //****
    public FinalQuestions ()
    {

    }


    //Methods

    Scanner scan = new Scanner(System.in);
    public void getEasy()
    {
        // ask first question


        while (millionaire=true)
        {
            System.out.println("Where is the Suez Canal?");
            answer1=scan.nextLine();

            if (answer1.equalsIgnoreCase("Egypt"))
            {
                System.out.println("Correct, you win $1000, proceed to question 2");
                millionaire=true;

            }
            else if (!answer1.equalsIgnoreCase("Egypt"))
            {
                System.out.println("Sorry, incorrect, you LOSE! Try again!! ");
                millionaire=false;
                break;
            }

            System.out.println("What is the symbol on the Periodic Table for Iron?");
            answer2=scan.nextLine();


            if (answer2.equalsIgnoreCase("Fe"))
            {
                System.out.println ("Correct, you win $5000, proceed to question 3");
                millionaire=true;
            }
            else if (!answer2.equalsIgnoreCase("Egypt"))
            {
                System.out.println("Sorry, incorrect, you LOSE! Try again!!");
                millionaire=false;
                break;
            }

            System.out.println("Which planet is closest to the sun?");
            answer3=scan.nextLine();

            if (answer3.equalsIgnoreCase("Mercury"))
            {
                System.out.println("Correct you win $10,000, proceed to question 4");
                millionaire=true;
            }
            else if (!answer3.equalsIgnoreCase("Mercury"))
            {
                System.out.println("Sorry, incorrect, you LOSE! Try again!!");
                millionaire=false;
                break;
            }

            System.out.println("How many U.S. states border the Gulf of Mexico?");
            answer4=scan.nextLine();

            if (answer4.equalsIgnoreCase("five"))
            {
                System.out.println("Correct you win $25,000, proceed to question 5");
                millionaire=true;
            }
            else if (!answer4.equalsIgnoreCase("five"))

            {
                System.out.println("Sorry, incorrect, you LOSE. Try again!!");
                millionaire=false;
                break;
            }

            System.out.println("What does the N stand for in NATO?");
            answer5=scan.nextLine();

            if (answer5.equalsIgnoreCase("north"))
            {
                System.out.println("Correct you win $50,000, and the game! Try the harder version now!");
                millionaire=true;
                break;

            }

            else if(!answer5.equalsIgnoreCase("north"))

            {
                System.out.println("Sorry, incorrect, you LOSE! Try again!!");
                millionaire=false;
                break;
            }

        }

        // save answers in array
        Q[0] = answer1;
        Q[1] = answer2;
        Q[2] = answer3;
        Q[3] = answer4;
        Q[4] = answer5;
    }
        public void getEasyAnswers()
        {
            for (int i=0; i<Q.length; i++)
            {

                System.out.println("Here are your answers: "+(Q[i]));
            }
        }


}
