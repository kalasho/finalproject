//**********************************
// Omar Kalash, Stephanie Vail, Allie Jones
// Final Project
// May5, 2015
// Purpose: To make a "Who Wants to be a Millionaire" type game.
//***********************************
import java.util.Date;
import java.util.Scanner;

public class FinalMain
{
    // Begin Main Method
    public static void main (String [] args)
    {
        // Label output with name and date:
        System.out.println ("Omar Kalash, Allie Jones, Stephanie Vail \nFinal Project\n" + new Date() + "\n");

        // Variable Directory:
        String question;
        String answer;
        int answer1;
        String choice;


        // Methods
        FinalQuestions easy = new FinalQuestions();

        HardQuestions hard = new HardQuestions();



        // Starting information
        Scanner s = new Scanner (System.in);
        System.out.println("Would you like to play easy or hard?");
        choice=s.next();



            if (choice.equalsIgnoreCase("easy"))
            {
                easy.getEasy();
                easy.getEasyAnswers();
            }
            else if (choice.equalsIgnoreCase("hard"))
            {
                hard.getHard();
                hard.getHardAnswers();
            }


    }
}

