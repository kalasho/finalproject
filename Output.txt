Output 1:
Omar Kalash, Allie Jones, Stephanie Vail 
Final Project
Sun Apr 26 21:17:09 EDT 2015

Would you like to play easy or hard?
easy
Where is the Suez Canal?
Egypt
Correct, you win $1000, proceed to question 2
What is the symbol on the Periodic Table for Iron?
Fe
Correct, you win $5000, proceed to question 3
Which planet is closest to the sun?
Mercury
Correct you win $10,000, proceed to question 4
How many U.S. states border the Gulf of Mexico?
Five
Correct you win $25,000, proceed to question 5
What does the N stand for in NATO?
North
Correct you win $50,000, and the game! Try the harder version now!
Here are your answers: Egypt
Here are your answers: Fe
Here are your answers: Mercury
Here are your answers: Five
Here are your answers: North

Output 2:
Omar Kalash, Allie Jones, Stephanie Vail 
Final Project
Sun Apr 26 21:17:09 EDT 2015

Would you like to play easy or hard?
easy
Where is the Suez Canal?
egypt
Correct, you win $1000, proceed to question 2
What is the symbol on the Periodic Table for Iron?
fe
Correct, you win $5000, proceed to question 3
Which planet is closest to the sun?
mercury
Correct you win $10,000, proceed to question 4
How many U.S. states border the Gulf of Mexico?
five
Correct you win $25,000, proceed to question 5
What does the N stand for in NATO?
north
Correct you win $50,000, and the game! Try the harder version now!
Here are your answers: egypt
Here are your answers: fe
Here are your answers: mercury
Here are your answers: five
Here are your answers: north

Output 3:
Omar Kalash, Allie Jones, Stephanie Vail 
Final Project
Sun Apr 26 21:19:51 EDT 2015

Would you like to play easy or hard?
easy
Where is the Suez Canal?
egypt
Correct, you win $1000, proceed to question 2
What is the symbol on the Periodic Table for Iron?
Fe
Correct, you win $5000, proceed to question 3
Which planet is closest to the sun?
Mercury
Correct you win $10,000, proceed to question 4
How many U.S. states border the Gulf of Mexico?
d
Sorry, incorrect, you LOSE. Try again!!
Here are your answers: egypt
Here are your answers: Fe
Here are your answers: Mercury
Here are your answers: d
Here are your answers: null

Output 4:
Omar Kalash, Allie Jones, Stephanie Vail 
Final Project
Sun Apr 26 21:21:28 EDT 2015

Would you like to play easy or hard?
hard
Where do the Broncos play football?
denver
Correct, you win $10000, proceed to question 2
Which ocean is the shallowest?
Arctic
Correct, you win $50000, proceed to question 3
What is the largest fresh water lake in North America?
lake SUPERIOR
Correct, you win $50000, proceed to question 3
What gas is the most common in the atmosphere?
oxygen
Sorry, incorrect, you LOSE. Try again! You're so close!
Here are your answers: denver
Here are your answers: Arctic
Here are your answers: lake SUPERIOR
Here are your answers: oxygen
Here are your answers: null

Output 5:
Omar Kalash, Allie Jones, Stephanie Vail 
Final Project
Sun Apr 26 21:22:30 EDT 2015

Would you like to play easy or hard?
hard
Where do the Broncos play football?
denver
Correct, you win $10000, proceed to question 2
Which ocean is the shallowest?
arctic
Correct, you win $50000, proceed to question 3
What is the largest fresh water lake in North America?
lake superior
Correct, you win $50000, proceed to question 3
What gas is the most common in the atmosphere?
nitrogen
Correct you win $250,000, proceed to question 5..It's time for the hardest question!
Where were the 1980 Winter Olympics held?
lake placid
Correct you win $500,000, and the game!
Here are your answers: denver
Here are your answers: arctic
Here are your answers: lake superior
Here are your answers: nitrogen
Here are your answers: lake placid
