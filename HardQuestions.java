//**********************************
// Omar Kalash, Stephanie Vail, Allie Jones
// Final Project
// May5, 2015
// Purpose: To make a "Who Wants to be a Millionaire" type game.
//***********************************
import java.util.Scanner;
import java.util.ArrayList;


public class HardQuestions
{
    String [] A = new String [5];
    boolean millionaire= true;
    String answer1, answer2, answer3, answer4, answer5;

    //****
    // Constructor
    //****
    public HardQuestions ()
    {

    }


    //Methods

    Scanner scan = new Scanner(System.in);
    public void getHard()

    {

        while (millionaire=true)
        {
            System.out.println("Where do the Broncos play football?");
            answer1=scan.nextLine();

            if (answer1.equalsIgnoreCase("Denver"))
            {

                System.out.println("Correct, you win $10000, proceed to question 2");
                millionaire=true;
            }
            else if (!answer1.equalsIgnoreCase("Denver"))

            {
                System.out.println("Sorry, incorrect, you LOSE. Try again!!");
                millionaire=false;
                break;
            }

            System.out.println("Which ocean is the shallowest?");
            answer2=scan.nextLine();
            if (answer2.equalsIgnoreCase("Arctic"))
            {
                System.out.println ("Correct, you win $50000, proceed to question 3");
                millionaire=true;
            }
            else if (!answer2.equalsIgnoreCase("Arctic"))
            {
                System.out.println("Sorry, incorrect, you LOSE");
                millionaire=false;
                break;
            }

            System.out.println("What is the largest fresh water lake in North America?");
            answer3=scan.nextLine();

            if (answer3.equalsIgnoreCase("lake superior"))
            {
                System.out.println("Correct, you win $50000, proceed to question 3");
                millionaire=true;
            }


            else if (!answer3.equalsIgnoreCase("lake superior"))
            {
                System.out.println("Sorry, incorrect, you LOSE. Try again! You can do this!");
                millionaire=false;
                break;
            }

            System.out.println("What gas is the most common in the atmosphere?");
            answer4=scan.nextLine();

            if (answer4.equalsIgnoreCase("Nitrogen"))
            {
                System.out.println("Correct you win $250,000, proceed to question 5..It's time for the hardest question!");
                millionaire=true;
            }

            else if (!answer4.equalsIgnoreCase("Nitrogen"))

            {
                System.out.println("Sorry, incorrect, you LOSE. Try again! You're so close!");
                millionaire=false;
                break;
            }

            System.out.println("Where were the 1980 Winter Olympics held?");
            answer5=scan.nextLine();

            if (answer5.equalsIgnoreCase("lake placid"))

            {
                System.out.println("Correct you win $500,000, and the game!");
                millionaire=true;
                break;
            }
            else if (!answer5.equalsIgnoreCase("lake placid"))
            {
                System.out.println("Sorry, incorrect, you LOSE. So close! Try again!");
                millionaire=false;
                break;
            }





        }

        A[0] = answer1;
        A[1] = answer2;
        A[2] = answer3;
        A[3] = answer4;
        A[4] = answer5;
    }
    public void getHardAnswers()
    {
        for (int i=0; i<A.length; i++)
        {

            System.out.println("Here are your answers: "+(A[i]));
        }

    }

}
